from nose.tools import assert_equal
import aims

def test_ints():
    numbers = [1,2,3,4,5]
    obs = round(aims.std(numbers), 5)
    exp = round(1.414213562,5)
    assert_equal(obs, exp)
