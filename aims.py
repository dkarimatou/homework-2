#!/usr/bin/env python
import math

def std(x):
	try:
		sumsquare=0.
		moy=0.
		n=len(x)
		mean=float(sum(x)/n)
		for j in range(n):
			sumsquare= sumsquare+ (x[j]-mean)**2
		return math.sqrt(sumsquare/n)

	except ZeroDivisionError:
		return 0
        except TypeError :
                return "The type of the elements must be Int or Float

	
def main():
	print"Welcome to the AIMS module\n"
	print "The standar deviation of [1,3,5,7,10] = ", std([1,3,5,7,10])
        
if __name__ == "__main__":
    main()
